﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Buttons : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void Play()
    {
        SceneManager.LoadScene(1);
    }

    void Load()
    {

    }

    public void Quit()
    {
        Application.Quit();
    }

    public void Cancel()
    {
        SceneManager.LoadScene((SceneManager.GetActiveScene().buildIndex + 1));
    }

    public void CustomCharacter()
    {
        Transform characterCreatorPanel = this.gameObject.transform.GetChild(1);

        Transform characterStatsPanel = this.gameObject.transform.GetChild(2);

        characterCreatorPanel.gameObject.SetActive(false);
        characterStatsPanel.gameObject.SetActive(true);
    }

    public void QuickCharacter()
    {
        
    }

    public void Portrait()
    {
        Transform characterStatsPanel = this.gameObject.transform.GetChild(2);

        Transform buttonPanel = this.gameObject.transform.GetChild(3);

        characterStatsPanel.gameObject.SetActive(false);
        buttonPanel.gameObject.SetActive(true);
    }

    public void Attributes()
    {
        Transform characterStatsPanel = this.gameObject.transform.GetChild(2);

        Transform buttonPanel = this.gameObject.transform.GetChild(4);

        characterStatsPanel.gameObject.SetActive(false);
        buttonPanel.gameObject.SetActive(true);
    }

    public void Skills()
    {
        Transform characterStatsPanel = this.gameObject.transform.GetChild(2);

        Transform buttonPanel = this.gameObject.transform.GetChild(5);

        characterStatsPanel.gameObject.SetActive(false);
        buttonPanel.gameObject.SetActive(true);
    }

    public void Feats()
    {
        Transform characterStatsPanel = this.gameObject.transform.GetChild(2);

        Transform buttonPanel = this.gameObject.transform.GetChild(6);

        characterStatsPanel.gameObject.SetActive(false);
        buttonPanel.gameObject.SetActive(true);
    }

    public void Name()
    {
        Transform characterStatsPanel = this.gameObject.transform.GetChild(2);

        Transform buttonPanel = this.gameObject.transform.GetChild(7);

        characterStatsPanel.gameObject.SetActive(false);
        buttonPanel.gameObject.SetActive(true);
    }
}
